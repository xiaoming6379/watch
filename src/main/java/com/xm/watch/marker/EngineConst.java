package com.xm.watch.marker;

/**
 * @Author xiaom
 * @Date 2020/6/22 11:02
 * @Version 1.0.0
 * @Description <>
 **/
public class EngineConst {

    public static final String _CLASSPATH                                = "classpath";
    public static final String _CLASSPATH_RESOURCE_LOAD_DEFAULT_PATH     = "classpath.resource.loader.class";
    public static final String _DEFAULT_INPUT_FILE_ENCODING              = "UTF-8";
    public static final String _DEFAULT_OUTPUT_FILE_ENCODING             = "UTF-8";

    public static final String _ENGINE_ENTITY_TEMPLATE_VM_DEFAULT_PATH   = "templates/Bean.vm";
    public static final String _ENGINE_DAO_TEMPLATE_VM_DEFAULT_PATH      = "templates/Repository.vm";

    public static final String _BASIS_PACKAGE_URL     = "com.xm.watch.entity";
    public static final String _BASIS_DAO_PACKAGE_URL = "com.xm.watch.dao.repository";
    public static final String _BASIS_META_INF_PATH   = "/META-INF/services/com.xm.watch.core.BeanGenerateProxyInterface";
    public static final String _BASIS_REPOSITORY_PATH = "/META-INF/services/com.xm.watch.dao.repository.BaseRepository";

}
