package com.xm.watch.marker;

import com.xm.watch.core.model.Annotation;
import com.xm.watch.core.model.JavaBean;
import com.xm.watch.core.model.ValNode;
import com.xm.watch.utils.CommonUtils;
import com.xm.watch.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.util.*;

/**
 * 模板引擎加载器
 */
public class EngineLoader {
    /**
     * 初始化模板引擎
     */
    public static VelocityEngine loadVelocityEngine(){
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, EngineConst._CLASSPATH);
        ve.setProperty(EngineConst._CLASSPATH_RESOURCE_LOAD_DEFAULT_PATH, ClasspathResourceLoader.class.getName());
        ve.setProperty("input.encoding", EngineConst._DEFAULT_INPUT_FILE_ENCODING);
        ve.setProperty("output.encoding", EngineConst._DEFAULT_OUTPUT_FILE_ENCODING);
        ve.init();
        return ve;
    }

    /**
     * 类名描述信息
     */
    public static VelocityContext initBeanAttr(String filePath, String packageUrl){
        VelocityContext velocityContext = new VelocityContext();

        JavaBean bean = new JavaBean();
        bean.setName(CommonUtils.upperCase(CommonUtils.underlineToHump(filePath)));
        bean.setLowerName(CommonUtils.getLowercaseChar(bean.getName()));
        bean.setPackageUrl(packageUrl);
        bean.setOriginalName(filePath);

        Annotation annotation = new Annotation();
        annotation.setAuthorMail("2210465185@qq.com");
        annotation.setAuthorName("xiaoming");
        annotation.setDate(DateUtils.formatYMDHMS(new Date()));

        velocityContext.put("bean",bean);
        velocityContext.put("annotation",annotation);

        return velocityContext;
    }

    /**
     * 设置类属性
     * @param valNodeList  属性字段集合
     */
    public static <T> void loadVelocityContext(VelocityContext velocityContext, List<ValNode> valNodeList){
        if(valNodeList == null){
            return;
        }

        velocityContext.put("attrList",valNodeList);
    }

    public static void loadVelocityPackageUrl(VelocityContext velocityContext, List<ValNode> valNodeList){
        if(valNodeList == null){
            return;
        }
        Set<String> packageUrlList = new HashSet<>();
        valNodeList.forEach(item -> {
            packageUrlList.add(item.getCls().getName());
        });
        velocityContext.put("packageList",new ArrayList<>(packageUrlList));
    }
}

