package com.xm.watch;

import com.xm.watch.core.MonitorTable;
import com.xm.watch.dao.BaseRepositoryFactoryBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.xm.watch"},
        repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class
)
@EntityScan("com.xm.watch.entity")
public class WatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(WatchApplication.class, args);
    }

    @Bean
    public DataLoader dataLoader() {
        return new DataLoader();
    }

    @Order
    static class DataLoader implements CommandLineRunner {

        @Override
        public void run(String... args) {
            new MonitorTable().start();
        }
    }
}
