package com.xm.watch.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

/**
 * @Author xiaom
 * @Date 2020/4/21 10:13
 * @Version 1.0.0
 * @Description <>
 **/
@NoRepositoryBean
public interface BaseRepository<T,TD extends Serializable> extends CrudRepository<T,TD>, JpaSpecificationExecutor<T> {

    int batchUpdate(Iterable<T> data);

    int batchSave(List<T> lists);

}
