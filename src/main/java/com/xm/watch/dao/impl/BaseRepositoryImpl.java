package com.xm.watch.dao.impl;

import com.xm.watch.dao.repository.BaseRepository;
import com.xm.watch.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @Author xiaom
 * @Date 2020/4/21 14:00
 * @Version 1.0.0
 * @Description <>
 **/
@Slf4j
public class BaseRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T,ID> implements BaseRepository<T,ID> {

    private final Integer _BATCH_SIZE = 500;

    private final EntityManager entityManager;
    private final JpaEntityInformation information;

    public BaseRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
        this.information = JpaEntityInformationSupport.getEntityInformation(domainClass, entityManager);
        this.entityManager = entityManager;
    }

    @Transactional
    @Override
    public int batchUpdate(Iterable<T> data) {
        Iterator<T> iterator = data.iterator();
        int index = 0;
        while (iterator.hasNext()){
            Optional<T> optionalT;
            T t = iterator.next();
            ID entityId = (ID) information.getId(t);
            if(StringUtils.isEmpty(entityId)){
                optionalT = Optional.empty();
            }else{
                optionalT = findById(entityId);
            }
            //获取空属性并处理成null
            String[] nullProperties = CommonUtils.getNullProperties(t);
            if(!optionalT.isPresent()){
                //如果数据库中不存在,则新增
                entityManager.persist(t);
            }else{
                //如果存在,则更新
                T target = optionalT.get();
                org.springframework.beans.BeanUtils.copyProperties(t, target, nullProperties);
                entityManager.merge(target);
            }
            index++;
            if (index % _BATCH_SIZE == 0){
                entityManager.flush();
                entityManager.clear();
            }
        }
        if (index % _BATCH_SIZE != 0){
            entityManager.flush();
            entityManager.clear();
        }
        return index;
    }

    @Transactional
    @Override
    public int batchSave(List<T> lists) {
        Iterator<T> iterator = lists.iterator();
        int index = 0;
        while (iterator.hasNext()){
            entityManager.persist(iterator.next());
            index++;
            if (index % _BATCH_SIZE == 0){
                entityManager.flush();
                entityManager.clear();
            }
        }
        if (index % _BATCH_SIZE != 0){
            entityManager.flush();
            entityManager.clear();
        }
        return index;
    }
}
