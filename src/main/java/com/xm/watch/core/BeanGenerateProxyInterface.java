package com.xm.watch.core;

/**
 * @Author xiaom
 * @Date 2020/6/22 14:16
 * @Version 1.0.0
 * @Description <>
 **/
public interface BeanGenerateProxyInterface<T> {

    /**
     * 获取SPI代理对象
     */
    T getProxyEntity();
}
