package com.xm.watch.core.source;

import com.alibaba.fastjson.JSON;
import com.xm.watch.context.SpringContext;
import com.xm.watch.core.classLoader.ClassGenerator;
import com.xm.watch.core.factory.DataSourceInterface;
import com.xm.watch.core.generate.CreateBeanImpl;
import com.xm.watch.core.generate.ICreate;
import com.xm.watch.core.model.FieldModel;
import com.xm.watch.core.model.ValNode;
import com.xm.watch.marker.EngineConst;
import com.xm.watch.utils.CommonUtils;
import com.xm.watch.utils.FileUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @Author xiaom
 * @Date 2020/6/22 9:26
 * @Version 1.0.0
 * @Description <>
 **/
@Slf4j
public class SourceHandler implements DataSourceInterface {

    private JdbcTemplate jdbcTemplate;
    @Getter
    private List<String> allTables = new ArrayList<>();
    @Getter
    private HashMap<String,List<FieldModel>> fieldMap  = new HashMap<>();
    private static HashMap<String,Class<?>> attrMapper = new HashMap<>();

    private static List<String> skipsList = Arrays.asList("mysql_version","sys_version");

    /**
     * 初始化mysql与java基础数据类型的映射关系
     */
    static {
        attrMapper.put("BIGINT",Long.class);
        attrMapper.put("DECIMAL",BigDecimal.class);
        attrMapper.put("INT",Integer.class);
        attrMapper.put("DATE",Date.class);
        attrMapper.put("VARCHAR",String.class);
    }

    public SourceHandler() throws Exception {
        this.jdbcTemplate = getConnection();
        if(this.jdbcTemplate == null){
            //todo:通过spring上下文容器获取IOC对象,可能存在对象实例化延迟问题. 后续使用<条件注解>解决此问题
            throw new Exception("Fetch mysql jdbc connection failed.");
        }
    }

    /**
     * 获取mysql link
     */
    private JdbcTemplate getConnection(){
        return SpringContext.getBean(JdbcTemplate.class);
    }

    /**
     * Get all table name.
     * @return this
     */
    public SourceHandler loadAllTableNames() throws SQLException {
        Assert.notNull(this.jdbcTemplate.getDataSource(),"Fetch mysql connection throw err.");
        Connection connection = this.jdbcTemplate.getDataSource().getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet tables = databaseMetaData.getTables(connection.getCatalog(), null, "%", new String[]{"TABLE"});
        while (tables.next()) {
            this.allTables.add(tables.getString("TABLE_NAME"));
        }
        connection.close();
        log.debug("All tables is:[{}]", JSON.toJSONString(allTables));
        return this;
    }

    /**
     * 通过表名获取字段名
     * @return this
     */
    public SourceHandler loadAllColumn() throws SQLException {
        if(this.allTables.size() == 0){
            log.warn("No table exists...");
            return this;
        }

        Assert.notNull(this.jdbcTemplate.getDataSource(),"Fetch mysql connection throw err.");
        Connection connection = this.jdbcTemplate.getDataSource().getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        log.debug("Start fetch table field name. All table size:[{}]",this.allTables.size());
        String columnName,columnType,dataType,columnSize,nullable;
        //获取字段名称
        for (String tableName : this.allTables) {
            ResultSet colRet = databaseMetaData.getColumns(null,"%", tableName,"%");
            List<FieldModel> fieldModels = new ArrayList<>();
            while(colRet.next()) {
                FieldModel model = new FieldModel();
                columnName = colRet.getString("COLUMN_NAME");
                if(skipsList.contains(columnName)){
                    continue;
                }
                columnType = colRet.getString("TYPE_NAME");
                dataType = colRet.getString("DATA_TYPE");
                columnSize = colRet.getString("COLUMN_SIZE");
                nullable = colRet.getString("NULLABLE");
                model.setColumn(columnName);
                model.setTypeCls(getJavaType(columnType));
                model.setDataType(dataType);
                model.setColumnSize(columnSize);
                model.setNullable(nullable);
                fieldModels.add(model);
            }
            this.fieldMap.put(tableName,fieldModels);
        }
        connection.close();
        log.debug("fileMap:{}",JSON.toJSONString(fieldMap));
        return this;
    }

    private Class<?> getJavaType(String type){
        Class<?> cls = attrMapper.get(type);
        return cls == null ? String.class : cls;
    }

    /**
     * 生成bean
     */
    public void generate(){
        if(this.fieldMap.keySet().size() == 0){
            log.error("No attributes can be generated");
            return;
        }

        ICreate create = new CreateBeanImpl();
        this.fieldMap.keySet().forEach(name -> {
            List<FieldModel> fieldModels = fieldMap.get(name);
            if(fieldModels.size() == 0){
                return;
            }
            String className = CommonUtils.upperCase(CommonUtils.underlineToHump(name));
            //生成java bean
            String javaPath = create.createEntity(name, EngineConst._ENGINE_ENTITY_TEMPLATE_VM_DEFAULT_PATH
                    ,transModel(fieldModels),CommonUtils.getEntityPath() + "/",EngineConst._BASIS_PACKAGE_URL);

            if(javaPath == null){
                return;
            }
            //编译java文件到classes目录
            ClassGenerator.compilerFile(javaPath);
            //生成dao
            try {
                String daoPath = create.createEntity(name + "Repository", EngineConst._ENGINE_DAO_TEMPLATE_VM_DEFAULT_PATH,
                        Collections.singletonList(new ValNode(Class.forName(EngineConst._BASIS_PACKAGE_URL + "." + className)))
                        ,CommonUtils.getEntityPath() + "/",EngineConst._BASIS_DAO_PACKAGE_URL);
                if(daoPath == null){
                    return;
                }
                ClassGenerator.compilerFile(daoPath);
            } catch (ClassNotFoundException e) {
                log.error("Create dao classes throw error.. e:{}",e.getMessage());
            }
            //追加SPI文件
            try {
                appendSPI(className,EngineConst._BASIS_META_INF_PATH,EngineConst._BASIS_PACKAGE_URL);
                appendSPI(className + "Repository",EngineConst._BASIS_REPOSITORY_PATH,EngineConst._BASIS_DAO_PACKAGE_URL);
            } catch (IOException e) {
                log.error("Append spi file throw error. ExMsg:{}",e.getMessage());
            }
        });

    }

    private void appendSPI(String className,String spiPath,String packageUrl) throws IOException {
        String spiFilePath = getResourcePath() + spiPath;
        String classUrl = packageUrl + "." +className;
        String context = FileUtils.readFileContent(spiFilePath);
        if(!"".equals(context)){
            Set<String> set = new HashSet<>(Arrays.asList(context.split("\n")));
            if(set.contains(classUrl) || set.contains(classUrl + "\r")){
                return;
            }
        }
        //Append
        FileUtils.appendFile(spiFilePath,classUrl);
    }

    private List<ValNode> transModel(List<FieldModel> fieldModels) {

        List<ValNode> valNodes = new ArrayList<>();
        fieldModels.forEach(item -> {
            ValNode valNode = new ValNode();
            valNode.setAttrName(CommonUtils.underlineToHump(item.getColumn()));
            valNode.setFieldType(item.getTypeCls().getSimpleName());
            valNode.setCls(item.getTypeCls());
            valNode.setOriginalName(item.getColumn());

            valNodes.add(valNode);
        });
        return valNodes;
    }

    public String getResourcePath() {
        return this.getClass().getResource("/").getPath();
    }

}
