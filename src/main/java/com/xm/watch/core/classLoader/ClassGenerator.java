package com.xm.watch.core.classLoader;

import javax.tools.*;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

/**
 * @Author xiaom
 * @Date 2020/6/22 15:41
 * @Version 1.0.0
 * @Description <>
 **/
public class ClassGenerator extends ClassLoader{

    /**
     * 使用java提供的编译器编译java code.
     */
    public static void compilerCode(String code){
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        StringSourceJavaObject sourceObject = new StringSourceJavaObject("Main", code);
        Iterable<? extends JavaFileObject> fileObjects = Arrays.asList(sourceObject);
        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, null, null, null, fileObjects);
        boolean result = task.call();
        if (result) {
            System.out.println("编译成功。");
        }
    }

    public static int compilerFile(String filePath){
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        return compiler.run(null, null, null, filePath);
    }

    static class StringSourceJavaObject extends SimpleJavaFileObject {
        private String content;

        /**
         * Construct a SimpleJavaFileObject of the given kind and with the
         * given URI.
         */
        protected StringSourceJavaObject(String name, String content) {
            super(URI.create("string:///" + name.replace('.','/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.content = content;
        }

        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            return content;
        }
    }

}
