package com.xm.watch.core.classLoader;

import java.util.HashMap;

/**
 * @Author xiaom
 * @Date 2020/6/22 17:45
 * @Version 1.0.0
 * @Description <>
 **/
public class BeanCache {

    private static HashMap<String,Object> tableBeanCache = new HashMap<>();

    public static void cacheBean(String key,Object bean){
        tableBeanCache.put(key,bean);
    }

    public static Object getBean(String key){
        return tableBeanCache.get(key);
    }

    public static HashMap<String,Object> getCache(){
        return tableBeanCache;
    }

    /**
     * 根据json文本属性匹配对应java bean.
     */
    public static Object getBeanByField(String json){
        return null;
    }
}
