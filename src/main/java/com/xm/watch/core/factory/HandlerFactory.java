package com.xm.watch.core.factory;

import com.xm.watch.core.source.SourceHandler;

/**
 * @Author xiaom
 * @Date 2020/6/22 9:25
 * @Version 1.0.0
 * @Description <数据源工厂>
 **/
public class HandlerFactory {

    private static final String _MYSQL_TYPE = "mysql";

    public static DataSourceInterface create(String type) throws Exception {
        switch (type){
            case _MYSQL_TYPE:
                return new SourceHandler();
            default:
                throw new Exception("No matching database type");
        }
    }

}
