package com.xm.watch.core;

import com.xm.watch.utils.CommonUtils;
import com.xm.watch.utils.ReflectionUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Optional;

/**
 * @Author xiaom
 * @Date 2020/6/24 11:07
 * @Version 1.0.0
 * @Description <>
 **/
@Slf4j
public class SqlHandler {

    private static final String _REPLACE_INFO = "REPLACE INFO ";

    private Object obj;
    private String tableName;
    private HashMap<String,Object> attrMap = new HashMap<>();
    @Getter
    private String sql;

    public SqlHandler(Object obj) {
        this.obj = obj;
        parserTableName();
        parserBeanAttributes();
    }

    /**
     * parser table name
     */
    private void parserTableName(){
        Table table = this.obj.getClass().getAnnotation(Table.class);
        if(table == null){
            //尝试根据类名当做表名
            this.tableName = CommonUtils.camelToUnderline(this.obj.getClass().getSimpleName(),1);
        }else{
            this.tableName = table.name();
        }
    }

    private void parserBeanAttributes(){
        this.attrMap = getAllAttributesAndValue(this.obj);
    }


    public SqlHandler assemblyInsertSql(){
        //拼接sql
        StringBuilder _sql = new StringBuilder(_REPLACE_INFO);
        _sql.append(tableName)
                .append("(");

        int i = 0,offset = 2;
        for (String column : this.attrMap.keySet()) {
            _sql.append("`");
            _sql.append(column);
            _sql.append("`");
            if(i <= this.attrMap.size() - offset){
                _sql.append(",");
            }
            i++;
        }
        i = 0;
        _sql.append(") VALUES(");
        for (Object value : this.attrMap.values()) {
            _sql.append("\"")
                    .append(value)
                    .append("\"");
            if(i <= this.attrMap.size() - offset){
                _sql.append(",");
            }
            i++;
        }
        _sql.append(")");
        log.info("Assembly insert sql:[{}]",_sql);
        this.sql = _sql.toString();
        return this;
    }


    /**
     * 获取对象所有属性与值
     * @return
     */
    public static HashMap<String,Object> getAllAttributesAndValue(@NonNull Object obj){
        Field[] fields = obj.getClass().getDeclaredFields();
        HashMap<String,Object> valMap = new HashMap<>();
        String key;
        Object value;
        for (Field field : fields) {
            value = ReflectionUtils.getFieldValue(obj,field.getName());
            if(value == null){
                continue;
            }
            if(field.getName().equals("id")){
                key = "id";
            }else{
                Column column = field.getAnnotation(Column.class);
                key = column.name();
            }
            valMap.put(key,value);
        }
        return valMap;
    }

}
