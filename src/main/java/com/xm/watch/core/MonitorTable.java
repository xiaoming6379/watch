package com.xm.watch.core;


import com.xm.watch.core.classLoader.BeanCache;
import com.xm.watch.core.factory.HandlerFactory;
import com.xm.watch.core.source.SourceHandler;
import com.xm.watch.dao.repository.BaseRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.ServiceLoader;

/**
 * @Author xiaom
 * @Date 2020/6/22 17:40
 * @Version 1.0.0
 * @Description <>
 **/
@Slf4j
public class MonitorTable extends Thread{

    /**
     * 刷新频率
     */
    private static final Integer _REFRESH_INTERVAL = 30000;

    @Override
    public void run() {
        log.info("Start monitor handler....");
        while (true){
            try {
                long start = System.currentTimeMillis();
                RefreshTable();
                RefreshBeanCache();
//                RefreshRepositoryCache();
                log.info("Refresh table data overTime:[{}]",(System.currentTimeMillis() - start));
                sleep(_REFRESH_INTERVAL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void RefreshBeanCache() {
        ServiceLoader<BeanGenerateProxyInterface> serviceLoader = ServiceLoader.load(BeanGenerateProxyInterface.class);
//        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) SpringContext.getApplicationContext().getAutowireCapableBeanFactory();
        serviceLoader.reload();
        String className;
        for (BeanGenerateProxyInterface animal : serviceLoader) {
            className = animal.getProxyEntity().getClass().getSimpleName();
            BeanCache.cacheBean(className, animal.getProxyEntity());
        }
    }

    private void RefreshRepositoryCache() {
        ServiceLoader<BaseRepository> serviceLoader = ServiceLoader.load(BaseRepository.class);
//        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) SpringContext.getApplicationContext().getAutowireCapableBeanFactory();
        serviceLoader.reload();
        String className;
        for (BaseRepository animal : serviceLoader) {
            log.info("Repository class name:${}",animal.getClass().getSimpleName());
        }
    }

    private void RefreshTable() throws Exception {
        SourceHandler handler = (SourceHandler) HandlerFactory.create("mysql");
        handler.loadAllTableNames()
                .loadAllColumn()
                .generate();
    }
}
