package com.xm.watch.core.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BaseGenerateNode {

    private List<ValNode> valNodes;

    private List<String> importPackageNames;
}
