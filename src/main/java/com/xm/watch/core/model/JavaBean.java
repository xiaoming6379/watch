package com.xm.watch.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author xiaom
 * @Date 2020/6/22 15:16
 * @Version 1.0.0
 * @Description <>
 **/
@Getter
@Setter
public class JavaBean {

    /** bean 名称 */
    private String name;
    /** bean 首字母小写名称 */
    private String lowerName;
    /** bean 路径 */
    private String beanUrl;
    /** package entity 路径 */
    private String packageUrl;
    /**原始名称**/
    private String originalName;

}
