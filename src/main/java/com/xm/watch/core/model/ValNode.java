package com.xm.watch.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author xiaom
 * @Date 2020/6/22 11:37
 * @Version 1.0.0
 * @Description <>
 **/
@Getter
@Setter
public class ValNode {

    /**
     * 原始字段名称
     */
    private String originalName;
    /**
     * 属性名 (驼峰命名)
     */
    private String attrName;
    /**
     * 数据类型
     */
    private String fieldType;
    /**
     * 默认值
     */
    private Object defaultVal = null;
    /**
     * 属性名首字母大写
     */
    private String flcapitalName;
    /**
     * 描述
     */
    private String desc;
    /**
     * 数据类型 class
     */
    private Class<?> cls;
    /**
     * 泛型类型
     */
    private String Generic;

    public ValNode(Class<?> cls) {
        this.cls = cls;
    }

    public ValNode(String generic) {
        Generic = generic;
    }

    public ValNode() {
    }
}
