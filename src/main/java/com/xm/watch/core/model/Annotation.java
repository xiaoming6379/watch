package com.xm.watch.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author xiaom
 * @Date 2020/6/22 15:15
 * @Version 1.0.0
 * @Description <>
 **/
@Getter
@Setter
public class Annotation {
    /**
     * 作者名称
     */
    private String authorName;
    /**
     * 作者邮箱
     */
    private String authorMail;
    /**
     * 日期
     */
    private String date;
    /**
     * 描述
     */
    private String version;

}
