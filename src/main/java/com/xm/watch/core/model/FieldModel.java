package com.xm.watch.core.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author xiaom
 * @Date 2020/6/22 10:13
 * @Version 1.0.0
 * @Description <>
 **/
@Getter
@Setter
public class FieldModel implements Serializable {

    /**
     * 列名
     */
    private String column;
    /**
     * 字段描述
     */
    private String desc;
    /**
     * 数据类型
     */
    private Class<?> typeCls;
    /**
     * 数据类型
     */
    private String dataType;
    /**
     * 字段大小
     */
    private String columnSize;
    /**
     * 是否为null
     */
    private String nullable;

}
