package com.xm.watch.core.generate;

import com.xm.watch.core.model.ValNode;
import com.xm.watch.marker.EngineLoader;
import com.xm.watch.utils.CommonUtils;
import com.xm.watch.utils.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class CreateBeanImpl implements ICreate {

    @Override
    public String createEntity(String fileName, String vmPath,List<ValNode> fieldList, String packageUrl, String outPath) {
        //获取模板
        Template template = loadTemplate(vmPath);
        //写入Java文件
        return writeJavaFile(fileName,outPath,fieldList,packageUrl,template);
    }

    private Template loadTemplate(String vmPath){
        VelocityEngine ve = EngineLoader.loadVelocityEngine();
        return ve.getTemplate(vmPath);
    }

    private <T> String writeJavaFile(String fileName, String outPath, List<ValNode> fieldList, String packageUrl, Template template){
        //初始化属性
        VelocityContext velocityContext = EngineLoader.initBeanAttr(fileName,outPath);
        //添加字段
        EngineLoader.loadVelocityContext(velocityContext,fieldList);
        //添加包路径属性
        EngineLoader.loadVelocityPackageUrl(velocityContext,fieldList);
        //写入字符串流
        StringWriter stringWriter = new StringWriter();
        String contextPath = buildFilePath(packageUrl, outPath);
        //使用模板加载
        template.merge(velocityContext, stringWriter);
        try {
            String className = CommonUtils.upperCase(CommonUtils.underlineToHump(fileName)) + ".java";
            FileUtils.newFile(contextPath,className,stringWriter.toString(),stringWriter.toString().length(),false);
            return contextPath + "/" + className;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String buildFilePath(String packageUrl, String outPath){
        return packageUrl + outPath.replaceAll("\\.","/");
    }
}
