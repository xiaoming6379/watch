package com.xm.watch.core.generate;


import com.xm.watch.core.model.BaseGenerateNode;
import com.xm.watch.core.model.ValNode;

import java.util.HashMap;
import java.util.List;

public interface ICreate {

    /**
     *
     * @param fileName   文件名称
     * @param fieldList  类属性集合
     * @param packageUrl 包路径
     * @param outPath    输入路径
     * todo:适用于需要动态生成字段属性
     */
    String createEntity(String fileName, String vmPath,List<ValNode> fieldList, String packageUrl, String outPath);
}
