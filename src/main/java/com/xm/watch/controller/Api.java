package com.xm.watch.controller;

import com.alibaba.fastjson.JSON;
import com.xm.watch.core.BeanGenerateProxyInterface;
import com.xm.watch.core.SqlHandler;
import com.xm.watch.core.classLoader.BeanCache;
import com.xm.watch.core.factory.HandlerFactory;
import com.xm.watch.core.source.SourceHandler;
import com.xm.watch.dao.impl.BaseRepositoryImpl;
import com.xm.watch.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @Author xiaom
 * @Date 2020/6/22 15:04
 * @Version 1.0.0
 * @Description <>
 **/
@RestController
public class Api {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("testTables")
    public Object testTables() throws Exception {
        SourceHandler handler = (SourceHandler) HandlerFactory.create("mysql");

        handler.loadAllTableNames()
                .loadAllColumn()
                .generate();

        return "OK";
    }

    @GetMapping("testSpi")
    public Object testSpi(){
        ServiceLoader<BeanGenerateProxyInterface> serviceLoader = ServiceLoader.load(BeanGenerateProxyInterface.class);
        Iterator<BeanGenerateProxyInterface> iterator = serviceLoader.iterator();
//        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) SpringContext.getApplicationContext().getAutowireCapableBeanFactory();
        while (iterator.hasNext()){
            BeanGenerateProxyInterface animal = iterator.next();
            Object obj = animal.getProxyEntity();
        }
        return "OK";
    }

    @GetMapping("save")
    public Object save(@RequestParam String json,@RequestParam String tableName){
        Object beanObj = BeanCache.getBean(CommonUtils.upperCase(CommonUtils.underlineToHump(tableName)));
        Assert.notNull(beanObj,"Not found tableName is :[" + tableName +"]");
        Object bean = JSON.parseObject(json,beanObj.getClass());
        jdbcTemplate.execute(new SqlHandler(bean).assemblyInsertSql().getSql());
        return "OK";
    }

    @GetMapping("testEntitySave")
    public Object testEntitySave(){
//        Test test = new Test();
//        test.setName("Save test 1");
//        baseRepository.save(test);
        return "OK";
    }

    @GetMapping("testSave")
    public Object testSave(){
        HashMap<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("username","testSpiUserName");
        jsonMap.put("phone","testSpiPhone");
        jsonMap.put("password","testSpiPassword");
        jsonMap.put("status",1);
        jsonMap.put("createTime","createTime");
        jsonMap.put("id",3);
        jsonMap.put("test","hahaha");
        jsonMap.put("ts2",520);

        String json = JSON.toJSONString(jsonMap);
        String tableName = "admin_user";

        Object beanObj = BeanCache.getBean(CommonUtils.upperCase(CommonUtils.underlineToHump(tableName)));

        Object bean = JSON.parseObject(json,beanObj.getClass());
        String sql = new SqlHandler(bean).assemblyInsertSql().getSql();
        jdbcTemplate.execute(sql);
        return "OK";
    }
}
