package com.xm.watch;

import com.alibaba.fastjson.JSON;
import com.xm.watch.context.SpringContext;
import com.xm.watch.core.BeanGenerateProxyInterface;
import com.xm.watch.core.factory.HandlerFactory;
import com.xm.watch.core.source.SourceHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Iterator;
import java.util.ServiceLoader;

@SpringBootTest
class WatchApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void testAllTables() throws Exception {
        SourceHandler handler = (SourceHandler) HandlerFactory.create("mysql");

        handler.loadAllTableNames()
                .loadAllColumn()
                .generate();
    }

    @Test
    public void testSpiProxy(){
        ServiceLoader<BeanGenerateProxyInterface> serviceLoader = ServiceLoader.load(BeanGenerateProxyInterface.class);
        Iterator<BeanGenerateProxyInterface> iterator = serviceLoader.iterator();
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory)SpringContext.getApplicationContext().getAutowireCapableBeanFactory();
        while (iterator.hasNext()){
            BeanGenerateProxyInterface animal = iterator.next();
            Object obj = animal.getProxyEntity();
            System.out.println(JSON.toJSONString(obj.toString()));
            System.out.println(obj.getClass());
            defaultListableBeanFactory.registerSingleton("User",obj);
        }
        System.out.println(SpringContext.getBean("User").toString());
    }

}
